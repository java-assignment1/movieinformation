package com.tw.movie;

import java.util.Arrays;

public class Movie {

    private final String title;
    private final String studio;
    private final String rating;

    public Movie(String title, String studio, String rating) {
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    public Movie(String title, String studio) {
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }

    public static Movie[] getPg(Movie[] movie) {
        Movie[] pgRatingMovie = new Movie[movie.length];
        int newIndex = 0;
        for (Movie value : movie) {
            if (value.rating.equals("PG")) {
                pgRatingMovie[newIndex++] = value;
            }
        }
        return pgRatingMovie;
    }

    public static void main(String[] args) {
        Movie[] movie = new Movie[3];
        movie[0] = new Movie("casino Royale", "Eon-productions", "PG-13");
        movie[1] = new Movie("RRR", "Eon-productions", "R");
        movie[2] = new Movie("Bahubali", "Eon-productions");

        System.out.println(Arrays.toString(Movie.getPg(movie)));
    }

    @Override
    public String toString() {
        return "title : " + title + " " + "studio : " + studio + " " + "rating : " + rating;
    }
}
